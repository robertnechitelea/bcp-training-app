
// PHYSICS CONSTANTS
const SPEED_CHANGE_PER_THRUST = 6;
const FUEL_CONSUMPTION_PER_THRUST = 0.2;
const ALTITUDE_CHANGE_PER_PHYSICS_STEP = 100;
const PI = 3.14;
const AIR_DENSITY_RHO = 1.27;

export const physicsStep = (centralFlightData) => {
        //Get the flight data before each physics step
        const { actualThrust, actualPitch, actualRoll } = centralFlightData.aircraftControlsActual;
        const { targetThrust, targetPitch, targetRoll } = centralFlightData.aircraftControlsTarget;

        //Aircraft orientations after physics step
        const thrust = handleThrustRealTime(actualThrust, targetThrust);
        const roll = handleRollRealTime(actualRoll, targetRoll);
        const pitch = handlePitchRealTime(actualPitch, targetPitch);

        // Grab flight and aircraft data
        const { altitude, distanceTravelled } = centralFlightData.flightStats;
          
        const aircraftSpeed = calculateSpeed(thrust, pitch);
        const liftCoefficient = calculateLiftCoefficient(pitch);

        const airPlaneFlightStats = calculateFlightStats(centralFlightData.aircraftStats, aircraftSpeed, liftCoefficient, thrust);
   
        return {
            ...centralFlightData,
            aircraftStats : {
                ...centralFlightData.aircraftStats,
                fuel : airPlaneFlightStats.leftFuel
            },
            aircraftControlsActual : {
                actualThrust : thrust,
                actualRoll : roll,
                actualPitch : pitch
            },
            flightStats : {
                speed : aircraftSpeed,
                altitude : altitude + airPlaneFlightStats.altitudeModifier < 0 ? 0 : altitude + airPlaneFlightStats.altitudeModifier,
                distanceTravelled : distanceTravelled + aircraftSpeed,
                liftCoefficient : liftCoefficient,
                liftForce : airPlaneFlightStats.liftForce,
                normalForce : airPlaneFlightStats.normalForce,
            }
        };
}

// TO DO : possibly refactor this function
const calculateFlightStats = (aircraftStats, airPlaneSpeed, liftCoefficient, thrust) => {
    const { weight, wingSurface, gravity, fuel} = aircraftStats;
    let altitudeModifier = 0;

    // Set an equilibrium range so that we can achieve level flight without gaining or losing altitude
    const EQUILIBRIUM_RANGE = [-400,400];

    const leftFuel = consumeFuel(fuel,thrust);

    const normalForce = (weight+leftFuel) * gravity;
    const liftForce = (1/2 * AIR_DENSITY_RHO * (airPlaneSpeed*airPlaneSpeed) * wingSurface * liftCoefficient).toFixed(2);
  
    // Change altitude based on the difference between forces
    if(isInEquilibriumRange(liftForce - normalForce, EQUILIBRIUM_RANGE)) altitudeModifier = 0;
    else if(liftForce - normalForce > EQUILIBRIUM_RANGE[1]) altitudeModifier+=ALTITUDE_CHANGE_PER_PHYSICS_STEP * liftCoefficient;
    else if(liftForce - normalForce < EQUILIBRIUM_RANGE[0]) altitudeModifier-=Math.abs(ALTITUDE_CHANGE_PER_PHYSICS_STEP * liftCoefficient);

    // Return multiple values for UI display
    return { altitudeModifier, liftForce, normalForce, leftFuel };
}

const handleRollRealTime = (actualRoll, targetRoll) => {
    return updateByStep(actualRoll, targetRoll, 5);
};
const handlePitchRealTime = (actualPitch, targetPitch) => {
    return updateByStep(actualPitch, targetPitch, 2);
};
const handleThrustRealTime = (actualThrust, targetThrust) => {
    return updateByStep(actualThrust, targetThrust, 1);
};

const calculateSpeed = (thrust, pitch) => {
    return thrust === 0 ? thrust * SPEED_CHANGE_PER_THRUST : thrust * SPEED_CHANGE_PER_THRUST - pitch;
};
const consumeFuel = (fuel, thrust) => {
    return fuel - thrust * FUEL_CONSUMPTION_PER_THRUST;
};
const calculateLiftCoefficient = (pitch) => {
    const coefficient = 2 * PI * pitch * PI/180.0 + 0.01;
    return coefficient.toFixed(2);
};

const updateByStep = (actual, target, step) => {
    if(actual < target) return parseInt(actual)+step;
    else if(actual > target) return parseInt(actual)-step;
    return actual;
};
const isInEquilibriumRange = (force, range) => {
    if(force > range[0] && force < range[1]) return true;
    return false;
};
