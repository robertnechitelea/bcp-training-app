import React from 'react';
import ControlPanel from "./Components/ControlPanel";
import './App.css';

function App() {
  return (
    <div className="App">
      <ControlPanel></ControlPanel>
    </div>
  );
}

export default App;
