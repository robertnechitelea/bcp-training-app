import React from 'react';
import {Container, Bottom, Top} from './cp_styles';
import {physicsStep} from "../physics/physics";

import ControlSliders from "./panel-elements/ControlSliders";
import FlightDisplayInfoArea from "./panel-elements/FlightDisplayInfoArea"; 
import PlaneStatsInput from "./panel-elements/PlaneStatsInput";
import ButtonsArea from "./panel-elements/ButtonsArea";
import AppInformation from "./panel-elements/AppInformation";


const PHYSICS_STEP_INTERVAL = 1000;

export default class ControlPanel extends React.Component{
    constructor(){
        super();
        this.state = {
            aircraftStats : {
                weight : 700,
                fuel : 250,
                wingSurface : 16,
                gravity : 9.98
            },
            aircraftControlsTarget : {
                targetPitch : 0,               
                targetRoll : 0,            
                targetThrust : 0,             
            },
            aircraftControlsActual : {
                actualPitch : 0,
                actualRoll : 0,
                actualThrust : 0
            },
            flightStats : {
                speed : 0,
                altitude : 0,
                distanceTravelled : 0,
                liftCoefficient : 0,
                liftForce : 0,
                normalForce : 0,
            }        
        }
    }

    componentDidMount(){
       this.timer = this.startRealtimePhysics();
    }

    componentWillUnmount(){
        clearInterval(this.timer);
    }

    startRealtimePhysics= () => {
        this.timer = setInterval(() => {
            this.physicsStep();   
        }, PHYSICS_STEP_INTERVAL);
    }

    physicsStep = () => {      
        let calculationAfterPhysicsStep = physicsStep(this.state);
        this.setState({
            ...calculationAfterPhysicsStep
        });
    }

    saveAirPlaneStats = (newStats) => {
        this.setState({
            aircraftStats : newStats
        });
    }

    sliderOnChange = (event) => {
        this.setState({
            ...this.state,
            aircraftControlsTarget : {
                ...this.state.aircraftControlsTarget,             
                [event.target.name] : event.target.value             
            }
        });  
    }

    render(){
        const { aircraftStats, aircraftControlsTarget, aircraftControlsActual, flightStats } = this.state;
        return(
            <Container>
                <AppInformation></AppInformation>
                <PlaneStatsInput saveCentralStats={this.saveAirPlaneStats} stats = {aircraftStats}></PlaneStatsInput>
                <Top>
                    <FlightDisplayInfoArea actualValues={aircraftControlsActual} weight={aircraftStats.weight} fuel={aircraftStats.fuel} flightStats={flightStats}></FlightDisplayInfoArea>
                </Top>
                <Bottom>
                    <ControlSliders sliderOnChange={this.sliderOnChange} controlStats={aircraftControlsTarget} actualStats={aircraftControlsActual}></ControlSliders> 
                    <ButtonsArea></ButtonsArea>
                </Bottom>
            </Container>
        )
    }
}