import styled from "styled-components";
import gaugeEffectLeft from '../../img/Asset 18.png';
import gaugeEffectRight from '../../img/Asset 23.png';
import gaugeEffectBottom from '../../img/Asset 21.png';
import gaugeEffectTop from '../../img/Asset 25.png';

export const Container = styled.div`
    top : 100px;
    display:flex;
    flex-direction : row;
    justify-content:space-around;
    align-items:center;
    width:100%;
`;

export const Gauge = styled.img`
    width : 480px;
    height : 440px;
    border-radius : 50%;
    transform: ${props => `rotate(${props.rotation}deg)`};
    transition : transform 1s linear;
`;

export const GaugeWrapper = styled.div`
    display:flex;
    align-items:center;
    justify-content:center;
    margin-bottom:15px;
`;

export const GaugeAndCursorWrapper = styled.div`
    display:flex;
    flex-direction:column-reverse;
    align-items:center;
    margin-top:100px;
`;

export const RotationCursor = styled.img`
    position:absolute;                               
    transform: ${props => `translateY(${-40 - props.pitch*4}%)`};
    z-index:1;
    transition : transform 1s linear;
`;

export const AltitudeMeter = styled.div`
    color:white;
    font-weight:bold;
    font-size:15px;
    background: #002890;
    margin: 0px 50px 0px 50px;
    height : 100%;
    width: 100%;
    border : 3px solid #002890;
    display : flex;
    justify-content: center;
    align-items:center;
    position : relative;
    opacity:0.3;
`;

export const AltitudeWrapper = styled.div`
    display:flex;
    flex-direction:column;
    height : 260px;
    width: 100px;
    margin:20px 20px 20px 20px;
    align-items:center;
`;

export const SpeedMeter = styled.div`
    margin: 0px 50px 0px 50px;
    height : 30%;
    width: 15%;
    border : 20px solid #00feff;
    display : flex;
    flex-direction:column;
    justify-content: center;
    align-content:space-between;
    border-radius:100%;
    margin-bottom:30px;
`;

export const SpeedMeterText = styled.div`
        font-size:38px;
        color: #00feff;    
        @import url(http://allfont.net/allfont.css?fonts=agency-fb);
         font-family: 'Agency FB', arial;
         font-weight:bold;
`;

export const Speed = styled.div`
        font-size:100px;
        color: #00feff;    
        @import url(http://allfont.net/allfont.css?fonts=agency-fb);
        font-family: 'Agency FB', arial;
        font-weight:bold;
`;

export const AltitudeFill = styled.div`
    background : #00feff;
    width : 100%;
    height : ${props => props.altitudePercentage > 100 ? 100 : props.altitudePercentage}%;
    position : relative;
    bottom : 0;
    position : absolute;
    bottom : 0;
    transition : width 0.2s linear;
`;

export const PhysicsStatsContentWrapper = styled.div`
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
    border : 2px solid #00feff;
    width:90%;
    height:90%;
    position:absolute;
    border-radius:15px;
    >p{
        font-size:20px;
        font-weight:bold
        }
`;

export const Title = styled.div`
    font-size:30px;
    text-transform: uppercase;
    font-weight:bold;
`;

export const PhysicsStatsBackground = styled.div`
    width : 100%;
    height : 100%;
    background : black;
    position:relative;
    border-radius:5%;
    align-items:center;
    display:flex;
    justify-content:center;
    opacity:0.7;
`;

export const PhysicsStatsWrapper = styled.div`
    display:flex; 
    justify-content:center; 
    align-items:center;
    margin: 40px 40px 40px 0;
    width : 450px;
    height : 340px;
`;

export const AltitudeText = styled.div`
    font-size:38px;
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
     font-family: 'Agency FB', arial;
     font-weight:bold;
`;

export const ContainerWrapperEffectLeft = styled.div`
    background-image:url(${gaugeEffectLeft});
    width:100%;
    height:100%;
    background-repeat:no-repeat;
`;

export const ContainerWrapperEffectRight = styled.div`
    background-image:url(${gaugeEffectRight});
    width:90px;
    height:455px;
    position:absolute;
    background-repeat:no-repeat;
    right:1px;
`;

export const Wrapper = styled.div`
    width:90px;
    height:455px;
    position:absolute;
    background-repeat:no-repeat;
    left:1px;
`;

export const ContainerWrapperEffectBottom = styled.div`
    background-image:url(${gaugeEffectBottom});
    width:900px;
    height:70px;
    background-repeat:no-repeat;
    position:absolute;
    margin-left:5%;
`;

export const ContainerWrapperEffectTop = styled.div`
    background-image:url(${gaugeEffectTop});
    width:900px;
    height:122px;
    background-repeat:no-repeat;
    position:absolute;
    margin-left:3%;
`;

export const FullGaugeWrapper = styled.div`
    width : 480px;
    height : 530px;
    display:flex;
    align-items:flex-start;
`;
