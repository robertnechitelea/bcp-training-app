import React from 'react';
import styled from 'styled-components';
//import slider from ''

const VerticalSlider = styled.input`
    writing-mode: bt-lr; /* IE */
    -webkit-appearance: slider-vertical; /* WebKit */
    width: 8px;
    height: 135px;
`;

const Container = styled.div`
    display:flex;
    justify-content:center;
    font-size:15px;
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
    font-weight:bold;
    text-transform: uppercase;
    z-index:2;
    width : 40%;
    height : 80%;
    align-items:center;
`;

const FillDiv = styled.div`
    height : 15px;
    width : 120px;
    background-image: linear-gradient(to right,#2F2F2F,#656565);
`;

const Filling = styled.div`
    margin : 0 auto;
    height : 15px;
    background-image: linear-gradient(to right,#29abe2,#00ffff);
    width : ${props => `${props.width}%`};
    transition : width 1s linear;
`;

const VerticalFillDiv = styled.div`
    height : 135px;
    width: 15px;
    display:inline-block;
    margin : 0px 10px;
    background-image: linear-gradient(to right,#29abe2,#00ffff);
`;

const VerticalFilling = styled.div`
    height : ${props => `${100 - props.actualThrust * 6.5}%`};
    background-image: linear-gradient(to right,#2F2F2F,#656565);
    transition : height 1s linear;

`;

const ControlSlidersBackground = styled.div`
    width:100%;
    height:100%;
    display:flex;
    background : black;
    opacity:0.4;
    border-radius:15px;
    display:flex;
    z-index:-1;
`;

const ControlSlidersWrapper = styled.div`
    display:flex;
    padding : 10px;
    font-size:15px;
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
    font-weight:bold;
    text-transform: uppercase;
    z-index:2;
    border:2px solid #00feff;
    border-radius:15px;
    position:absolute;
    width: 35%;
    height: 25%;
`;
const ControlWrapper = styled.div`
     display:flex;
     width:70%;
     align-items:center;
     justify-content:center;
         >div>p{    
            font-size:20px;
            color: #00feff;    
            @import url(http://allfont.net/allfont.css?fonts=agency-fb);
            font-family: 'Agency FB', arial;
            font-weight:bold;
    }
`;

const ThrustWrapper = styled.div`
        display:flex;
        width:30%;
        justify-content:center;
             >div>p{    
            font-size:20px;
            color: #00feff;    
            @import url(http://allfont.net/allfont.css?fonts=agency-fb);
            font-family: 'Agency FB', arial;
            font-weight:bold;
    }
`;

const ControlSliders = ({controlStats, actualStats, sliderOnChange}) => {
    return(
        <Container>{/*done*/}
            <ControlSlidersBackground/>{/*done*/}
            <ControlSlidersWrapper>{/*done*/}
                <ControlWrapper>
                    <div>
                        <p>Control Pitch</p>
                        <p>{controlStats.targetPitch} degrees</p>
                        <input type="range" min="-16" max="16" step="2" onChange={(e) => sliderOnChange(e)} name="targetPitch" style={{width:'120px'}} value={controlStats.targetPitch}></input>
                        <FillDiv>
                            <Filling width={5 * Math.abs(actualStats.actualPitch)}></Filling>
                        </FillDiv>
                    </div>
                    <div>
                        <p>Control Aileron</p>
                        <p>{controlStats.targetRoll} degrees</p>
                        <input type="range" min="-40" max="40" step="5" onChange={(e) => sliderOnChange(e)} name="targetRoll" style={{width:'120px'}} value={controlStats.targetRoll}></input>
                        <FillDiv>
                            <Filling width={2.5 * Math.abs(actualStats.actualRoll)}></Filling>
                        </FillDiv>
                    </div>
                </ControlWrapper>
                <ThrustWrapper>
                        <div>
                        <p>Aircraft Thrust</p>
                        <p>{controlStats.targetThrust} level</p>
                        <VerticalSlider type="range" min="0" max="15" onChange={(e) => sliderOnChange(e)} name="targetThrust" value={controlStats.targetThrust}></VerticalSlider>
                        <VerticalFillDiv>
                            <VerticalFilling actualThrust={actualStats.actualThrust}></VerticalFilling>
                        </VerticalFillDiv>
                        </div>
                </ThrustWrapper>
            </ControlSlidersWrapper>
        </Container>
    )
}

export default ControlSliders;