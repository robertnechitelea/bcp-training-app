import React from 'react';
import {Container,
        SpeedMeter,
        SpeedMeterText,
        Speed,RotationCursor,
        Gauge,AltitudeMeter,
        AltitudeFill,
        PhysicsStatsContentWrapper,
        PhysicsStatsBackground,
        PhysicsStatsWrapper,Title,
        AltitudeWrapper,
        AltitudeText,
        ContainerWrapperEffectLeft,
        ContainerWrapperEffectRight,
        ContainerWrapperEffectBottom,
        Wrapper,
        GaugeWrapper,
        GaugeAndCursorWrapper,
        ContainerWrapperEffectTop,
        FullGaugeWrapper,
        } from './style';
import rotationGauge from '../../img/c1.png';
import rotationCursor from "../../img/rotation_cursor.png";

const FlightDisplayInfoArea = ({actualValues, flightStats, weight, fuel}) => {
    return(
        <Container>
            <Wrapper>
            <ContainerWrapperEffectLeft/></Wrapper>
            <SpeedMeter>
                <SpeedMeterText>Speed</SpeedMeterText>
                <Speed>{flightStats.speed}m/s</Speed>
            </SpeedMeter>
            <FullGaugeWrapper>
                <ContainerWrapperEffectTop/>
            <GaugeAndCursorWrapper>
                <GaugeWrapper>
                    <RotationCursor pitch={actualValues.actualPitch}  src={rotationCursor}/>
                     <Gauge rotation={actualValues.actualRoll} src={rotationGauge}/>
                </GaugeWrapper>
                <ContainerWrapperEffectBottom/>
            </GaugeAndCursorWrapper>
            </FullGaugeWrapper>
            <AltitudeWrapper>
            <AltitudeText>Altitude:
                {flightStats.altitude}m
            </AltitudeText>
            <AltitudeMeter>
                <AltitudeFill altitudePercentage={0.01 * flightStats.altitude}>
                </AltitudeFill>
            </AltitudeMeter>
            </AltitudeWrapper>
            <PhysicsStatsWrapper>
            <PhysicsStatsBackground>
                <PhysicsStatsContentWrapper>
                        <Title>Flight Stats</Title>
                        <p>Weight Total : {parseInt(weight + fuel)}</p>
                        <p>Fuel : {parseInt(fuel)}</p>
                        <p>Lift Coefficient : {flightStats.liftCoefficient}</p>
                        <p>Lift Force {parseInt(flightStats.liftForce)} Newtons</p>
                        <p>Gravity Force {parseInt(flightStats.normalForce)} Newtons</p>
                        <p>Distance {(flightStats.distanceTravelled/1000).toFixed(2)} km</p>
                </PhysicsStatsContentWrapper>
            </PhysicsStatsBackground>
            </PhysicsStatsWrapper>
            <ContainerWrapperEffectRight/>
        </Container>

    )
}

export default FlightDisplayInfoArea;