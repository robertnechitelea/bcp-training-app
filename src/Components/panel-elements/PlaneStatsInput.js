import React, {useState} from 'react';
import styled from 'styled-components';
import airplane from '../../img/airplane.png';
import modalBackground from '../../img/Asset15.png';
import launchButton from '../../img/Asset16.png';

const Overlay = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(40,40,40,0.8);
    z-index:10;
`

const StatsPanel = styled.div`
    background-image:url(${modalBackground});
    background-repeat: no-repeat;
    width : 850px;
    height : 350px;
    margin : 200px auto;
    border-radius : 25px;
    padding : 50px;
    border:10px solid #00FEFF;
`;

const SaveBtn = styled.button`
    background-image:url(${launchButton});
    background-repeat:no-repeat;
    height:58px;
    width:250px;
    cursor:pointer;
    border-radius:10px;
    border:5px solid #32527B;
`;

const Airplane = styled.div`
    >img{    
        width:440px;
        height:385px;
    };
`;

const Header = styled.div`
    display:flex;
    flex:1;
    align-items:center;
    justify-content:center;
`;
const Title = styled.div`
    font-size:60px;
    color:#00FEFF; 
    font-weight:bold;
    text-transform: uppercase;
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
`;

const Menu = styled.div`
    display:flex;
`;

const RightSpecs = styled.div`
>label{
    color:white;
    font-weight:bold;
}
`;

const LeftSpecs = styled.div`
    margin-left:20px;
    >label{
        color:white;
        font-weight:bold;
    }
`;

const Specs = styled.div`
    display: flex;
    height:60%;
    align-items:center;
`;

const Launch =styled.div`
    display:flex;
    justify-content:flex-end;
    align-items:flex-end;
    >button {
        color:white;
        font-weight:bold;
        justify-content:center;
    }
`;

export const TextWrapper = styled.div`
    color:#00FEFF;
`;


const PlaneStatsInput = ({saveCentralStats, stats}) => {
    const [showPanel, setIsPanelShowing] = useState(true);
    const [innerStats, setInnerStats] = useState(stats);

    const saveStatsAndClose = () => {
        setIsPanelShowing(false);
        saveCentralStats(innerStats);
    }

    const handleChange = (evt) => {
        const value = evt.target.value;
        setInnerStats({
            ...innerStats,
            [evt.target.name]: value
        });
    }

    return(
        showPanel ?
            <Overlay>
                <StatsPanel>
                    <Header>
                        <Title>Airplane Stats</Title>
                    </Header>
                    <Menu>
                        <Airplane>
                            <img src={airplane} alt="avion"></img>
                        </Airplane>
                        <div>
                            <Specs>
                                <RightSpecs >
                                    <TextWrapper>Gravity Settings</TextWrapper><br/>
                                    <input onChange={handleChange} type="number" name="gravity" value={innerStats.gravity}/><br/>

                                    <TextWrapper>Fuel Capacity(liters)</TextWrapper><br/>
                                    <input onChange={handleChange} type="number" name="fuel" value={innerStats.fuel}/><br/>

                                </RightSpecs>

                                <LeftSpecs>
                                    <TextWrapper>Airplane Weight(kg)</TextWrapper><br/>
                                    <input onChange={handleChange} type="number" name="weight" value={innerStats.weight}/><br/>

                                    <TextWrapper>Airplane Wing Surface(m^2)</TextWrapper><br/>
                                    <input onChange={handleChange} type="number" name="wingSurface" value={innerStats.wingSurface}/><br/>
                                </LeftSpecs>
                            </Specs>
                            <Launch>
                                <SaveBtn onClick={saveStatsAndClose}/>
                            </Launch></div>
                    </Menu>

                </StatsPanel>
            </Overlay> : null
    )
}

export default PlaneStatsInput;