import React,{useState} from 'react';
import styled from 'styled-components';

const Container = styled.div`
    width : 100%;
`;

const InfoImage = styled.img`
    width : 80px;
    height : 80px;
    position : fixed;
    bottom : 0;
    left : 0;
    :hover{
        transform:scale(1.1);
    }

    transition : transform 0.3s linear;
`;

const InformationPanel = styled.div`
    box-shadow: 0px 0px 14px -4px rgba(0,0,0,0.75);
    border-radius : 10px;
    width : 600px;
    height : 700px;
    background : white;
    position : fixed;
    left : 30%;
    top : 50px;
    z-index : 1000;   
    padding : 30px;
`;

const InfoText = styled.div`
    text-align : left;
`;

const CloseButton = styled.div`
    width: 20px;
    height: 20px;
    background: #ff3939;
    border-radius: 50%;
    padding: 3px;
    color: white;
    position: absolute;
    right: 10px;
    text-align: center;
    font-weight: bold;
    top: 10px;
    :hover{
        cursor : pointer;
        background : #ff5b1d;
    }
`;
const AppInformation = () => {
    const [showInfoPanel, setShowInfoPanel] = useState(false)
    return(
        <Container>
            <InfoImage onClick={() => setShowInfoPanel(true)} src="https://img.icons8.com/clouds/100/000000/information.png"></InfoImage> 
            {showInfoPanel && 
                <InformationPanel>
                    <h3>App Information</h3>
                    <InfoText>
                        <CloseButton onClick={() => setShowInfoPanel(false)}>X</CloseButton>
                        <p> The app tries to simulate a very basic approach to flying based on two main forces : lift force and gravity force.
                            If the lift force is greater than the gravity force then the airplane takes off the ground and starts flying.</p>
                        <p>
                            The normal force(when the plane is not moving) is determined by a few factors such as : weight, fuel weight and gravity.
                        </p>
                        <p>
                            The lift is determined by : the lift coefficient, the aircraft speed, air density and wing surface area. Changing the pitch
                            of the aircraft changes the lift coefficient.
                        </p>

                        <strong>Instructions</strong>
                        <p><strong>Control Pitch</strong> : change the rotation of the aicraft so that the tip of it points up or down relative to the horizon.
                            Changing this affects the lift coefficient and the amount of altitude gained every second.
                        </p>
                        <p><strong>Control Aileron</strong> : change the incliation of the aircraft left/right.
                        </p>
                        <p><strong>Aircraft Thrust</strong> : even though physically inaccurate, increasing the slider will increase the speed of the aircraft. The Thrust
                        influences the amount of fuel consumed, the distance travelled per second and the final amount of lift force generated.
                        </p>

                    </InfoText>
                </InformationPanel>}
        </Container>
    )
}

export default AppInformation;


