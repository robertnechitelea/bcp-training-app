import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    display:flex;
    justify-content:center;
    font-size:15px;
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
    font-weight:bold;
    text-transform: uppercase;
    z-index:2;
    width : 40%;
    height : 80%;
    align-items:center;
`;

const ButtonsAreaBackground = styled.div`
    width:100%;
    height:100%;
    display:flex;
    background : black;
    opacity:0.4;
    border-radius:15px;
    display:flex;
    z-index:-1;
`;

const ContentWrapper = styled.div`
    display:flex;
    flex-direction:column;
    padding : 10px;
    font-size:15px;
    color: #00feff;    
    @import url(http://allfont.net/allfont.css?fonts=agency-fb);
    font-family: 'Agency FB', arial;
    font-weight:bold;
    text-transform: uppercase;
    z-index:2;
    border:2px solid #00feff;
    border-radius:15px;
    position:absolute;
    width: 35%;
    height: 25%;
`;

const ButtonWrapper = styled.div`
    display:flex;
        >button{
        background-color:#00feff;
        margin:5px;
        border-color:black;
        width:70px;
        height:35px;
        color: black;    
        @import url(http://allfont.net/allfont.css?fonts=agency-fb);
        font-family: 'Agency FB', arial;
        font-size:20px;
        font-weight:bold;
        cursor:pointer;
        &:hover{
            border:5px solid #00feff;
             }
`;

export const Content = styled.div`

`;

const ButtonsArea = (props) => {
    return(
            <Container>
            <ButtonsAreaBackground/>
            <ContentWrapper>
                <h1>Future controls area</h1>
                <ButtonWrapper>
                    <button>F1</button>
                    <button>F2</button>
                    <button>F3</button>
                    <button>F4</button>
                    <button>F5</button>
                    <button>F6</button>
                    <button>F7</button>
                    <button>F8</button>
                </ButtonWrapper>
            </ContentWrapper>
            </Container>
    )
}

export default ButtonsArea;