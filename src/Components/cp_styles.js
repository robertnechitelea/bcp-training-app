import styled from 'styled-components';
import backgroundImage from '../img/Background.png';
import gaugeEffect from "../img/Asset 25.png";

import gaugeEffectLeft from "../img/Asset 19.png";
import gaugeEffectRight from "../img/Asset 24.png";

export const Container = styled.div`
    display:flex;
    flex-direction:column;
    background-image: url(${backgroundImage});
    background-repeat:no-repeat;
    background-size:cover;
    background-size: cover; 
    -ms-background-size: cover; 
    -o-background-size: cover; 
    -moz-background-size: cover; 
    -webkit-background-size: cover;
    height:100vh;
    width:100vw;
`;

export const Top = styled.div`
    display : flex;
    justify-content : center;
    height:60%;
    width:100%;
`;

export const Bottom = styled.div`
    padding-top : 10px;
    display : flex;
    justify-content : space-around;
    height:40%;
    width:100%;
`;