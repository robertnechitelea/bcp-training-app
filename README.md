##### Note : To solve the exercises from the Best Coding Practices training please make a branch out of the 'training' branch.

# Welcome to BCP - Training - App!

Hi! 
BCP stands for Best Coding Practices and this app's purpose is to act as a training support where you can practice anything learned throughout the training sessions. Feel free to create a new branch and experiment with modifying the code - add your own features or even improve the existing code!
For any changes to the codebase please open a pull request and write a small description of what your change does.


# Flight App - How To
The application is a basic flight simulator which can provide a rough simulation of flight forces acting on the plane. You can easily extend or modify any of the implemented physics or introduce new ones. Currently these factors are taken into account when piloting a plane : 

- Aircraft Stats  : weight, fuel capacity, wingspan

- Physics : gravity forces, air density, lift coefficient

- Aircraft Controls : pitch, thrust, roll

- Effects of flight : altitude, speed, distance travelled